from skimage import measure
import skimage.io as skio
import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import skeletonize
from scipy.ndimage import gaussian_filter
from skimage import data
from skimage import img_as_float
from skimage.morphology import reconstruction, dilation, closing, opening, convex_hull_image
from skimage.morphology import disk
import matplotlib.colors as mcolors

footprint = disk(5)

qualCol = plt.cm.viridis(np.random.permutation(np.linspace(0,1,21)))
qualCol[0] = [0,0,0,0]
qualColorMap = mcolors.LinearSegmentedColormap.from_list('my_qualitative_colormap', qualCol)

image = skio.imread("france.png")
image = img_as_float(image)

plt.axis('off')
plt.imshow(image, cmap='gray')
plt.imsave('plotForPresentation/00_OriginalImage.png', image, cmap='gray')
plt.clf()

imagega = gaussian_filter(image, 20)

plt.axis('off')
plt.imshow(imagega, cmap='gray')
plt.imsave('plotForPresentation/01_SmoothImage.png', imagega, cmap='gray')
plt.clf()

seed = np.copy(imagega)
seed[1:-1, 1:-1] = imagega.min()
mask = imagega

imagegamore = reconstruction(seed, mask, method='dilation')

plt.axis('off')
plt.imshow(imagegamore, cmap='gray')
plt.imsave('plotForPresentation/02_SmootherImage.png', imagegamore, cmap='gray')
plt.clf()

imagega = imagega - imagegamore

plt.axis('off')
plt.imshow(imagega, cmap='gray')
plt.imsave('plotForPresentation/03_DifferenceImage.png', imagega, cmap='gray')
plt.clf()

imagebin = imagega > np.quantile(imagega, q=0.975)

plt.axis('off')
plt.imshow(imagebin, cmap='gray')
plt.imsave('plotForPresentation/04_ThresholdedImage.png', imagebin, cmap='gray')
plt.clf()

blobs_labels = measure.label(imagebin, background=0)
us, cs = np.unique(blobs_labels, return_counts=True)

plt.axis('off')
plt.imshow(blobs_labels, cmap=qualColorMap)
plt.imsave('plotForPresentation/05_LabeledComponentImage.png', blobs_labels, cmap=qualColorMap)
plt.clf()

for u, c in zip(us, cs):
    if c < np.quantile(cs, q = 0.5):
        blobs_labels[blobs_labels==u] = 0

plt.axis('off')
plt.imshow(blobs_labels, cmap=qualColorMap)
plt.imsave('plotForPresentation/05_LabeledComponentImageNoSmall.png', blobs_labels, cmap=qualColorMap)
plt.clf()

plt.axis('off')
plt.imshow(image, cmap='gray')
plt.imshow(blobs_labels, alpha=0.5, cmap=qualColorMap)
plt.savefig('plotForPresentation/06_LabelOverlayTransparentImage.png', dpi=800)
plt.clf()

plt.axis('off')
masked = np.ma.masked_where(blobs_labels == 0, blobs_labels)
plt.imshow(image, 'gray', interpolation='none')
plt.imshow(masked, qualColorMap, interpolation='none', alpha=0.75)
plt.savefig('plotForPresentation/07_LabelOverlayMaskedImage.png', dpi=800)
plt.clf()

plt.axis('off')
imagega = gaussian_filter(image, 5)
skel = skeletonize(imagega > np.quantile(imagega, q=0.9))
skeldi = dilation(skel, footprint)
plt.imsave('plotForPresentation/08_Skeleton.png', skeldi, cmap="gray")

quit()
